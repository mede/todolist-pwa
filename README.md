# TodoList 

L'aplication TodoList possèdent les fonctionnalitées suivantes:

- Ajout de tache avec un titre et une description et un état réalisé
- Possibilité de créer et lire des todos offline
- Possède une page de détail de todo
- Les todos possèdent une date de début et de fin 

Libs

IndexedDB (https://github.com/jakearchibald/idb)

